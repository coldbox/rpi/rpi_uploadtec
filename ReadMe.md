# Upload Tec Script

This Code is for uploading the [TECware](https://gitlab.psi.ch/coldbox/tec/tecware) to the TEC in the Coldbox environemen via the CAN Bus.

## Installation

- Please install python 3.6 on you PC / Raspberry PI
- then go to you terminal, jump to this directory
- go into the python directory: `cd python`
- install python library: `python setup.py install`
- then go back to the home directory
- copy the srec file from the [TECware](https://gitlab.psi.ch/coldbox/tec/tecware) repo to this directory
- (If you are on windows, please change you CAN Interface in the uploadtec.py script) [here](https://www.feaser.com/en/blog/2018/07/libopenblt-python-bindings-tutorial/) some infos

### Install the CAN interface (for RPi CAN Hat)

Add the following line to your /boot/config.txt file:
`dtoverlay=mcp2515-can0,oscillator=12000000,interrupt=25,spimaxfrequency=2000000`

`sudo reboot`

`pip3 install python-can`
... for python module

maybe edit permissions
`./resetCAN.sh`
... for starting can0

## Usage

to use this script the libopenblt.(so on linux)(dll on windows) library should be in the same directory.

on linux first reset the canbus with `./resetCAN.sh`.

now copy the .srec file to this directory

the use the script to upload the code:
`python uploadtec.py [all/1-8] [filename.srec]`

if the upload gets interrupted, the should restart the upload because th code on the TEC is now corrupted.
The Bootloader itself should not corrupt itself, so everything is save. These are 2 completly different and independent programms.
