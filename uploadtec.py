# This is a sample Python script.

import openblt as blt
import sys


LINE_UP = '\033[1A'
LINE_CLEAR = '\x1b[2K'

def print_del(txt:str):
	"""print and delete line before"""
	print(LINE_UP, end=LINE_CLEAR)
	print(txt)


def upload_code(tec_id, filename):
    blt.firmware_init(blt.BLT_FIRMWARE_PARSER_SRECORD)
    session_type = blt.BLT_SESSION_XCP_V10
    session_settings = blt.BltSessionSettingsXcpV10()
    transport_type = blt.BLT_TRANSPORT_XCP_V10_CAN
    transport_settings = blt.BltTransportSettingsXcpV10Can()
    transport_settings.baudrate = 125000
    transport_settings.receiveId = 0x340 | tec_id
    transport_settings.transmitId = 0x300 | tec_id
    transport_settings.deviceName = 'can0'

    blt.session_init(session_type, session_settings,transport_type, transport_settings)
    
    print('tec: ' + str(tec_id))
    print('connecting to the bootloader')

    while blt.session_start() != blt.BLT_RESULT_OK:
        pass
        
    print('connected')
    print('clearing [0/0]')
    
    if blt.firmware_load_from_file(filename) != blt.BLT_RESULT_OK:
        print('error reading file')
        sys.exit()

    segment_count = blt.firmware_get_segment_count()
    for segment_idx in range(0, segment_count):
        print_del(f"clearing [{segment_idx+1}/{segment_count}]")
        segment_data, segment_address, segment_len = blt.firmware_get_segment((segment_idx))
        res = blt.session_clear_memory(segment_address, segment_len)
        if res != blt.BLT_RESULT_OK:
            print('error clearing data')
            sys.exit()
        
        
    print("uploading [0/0]")

    for segment_idx in range(0, segment_count):
        print_del(f"uploading [{segment_idx+1}/{segment_count}]")
        segment_data, segment_address, segment_len = blt.firmware_get_segment((segment_idx))
        res = blt.session_write_data(segment_address, segment_len, segment_data)
        if res != blt.BLT_RESULT_OK:
            print('error writing data')
            sys.exit()
    

    print("finished upoading")

    blt.session_stop()

    blt.session_terminate()
    blt.firmware_terminate()
    
    print("disconnected")


if __name__ == '__main__':

    if len(sys.argv) != 3:
        print('check parameters: [1-8/all] [filename]')
        sys.exit(1)
    
    if sys.argv[1] == 'all':
        for i in range(8):
            upload_code(i+1, sys.argv[2])
        sys.exit(0)
        
    try:
        tec_id = int(sys.argv[1])
    except:
        print("error: illegal command (tecid)")
        print('check parameters: [1-8/all] [filename]')
        sys.exit(1)
        
    if( 1 < tec_id > 8):
        print("wrong tec id")
        print('check parameters: [1-8/all] [filename]')
        sys.exit(1)
    
    upload_code(tec_id, sys.argv[2])

